<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 29/12/2015
 * Time: 11:25
 */

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    require_once("../config/db_connect.php");
    require_once("../phpmailer/PHPMailerAutoload.php");

    $name   = strip_tags(trim($_POST["name"]));
    $name   = str_replace(array("\r", "\n"), array(" ", " "), $name);
    $phone  = strip_tags(trim($_POST["phone"]));
    $phone  = str_replace(array("\r", "\n"), array(" ", " "), $phone);
    $email  = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);

//    //prepare/bind
    $stmt = $link->prepare("INSERT INTO newTable (name, email, phone) VALUES (:name, :email, :phone)");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':phone', $phone);
    //insert
    $stmt->execute();

    //phpmailer
    $mail = new PHPMailer;
    // $mail->SMTPDebug = 4;
    // $mail->Debugoutput = 'html';
    $mail->isSMTP();
    $mail->Host = 'smtp.postmarkapp.com';
    $mail->SMTPAuth = true;
    $mail->Username = '';
    $mail->Password = '';
    $mail->SMTPSecure = 'TLS';
    $mail->Port = 587;

    //$mail->isSendmail();
    $mail->CharSet = 'UTF-8';
    $mail->setFrom('', '');
    $mail->addAddress($email, $name);
    $mail->addReplyTo('', '');

    $emailOutput = file_get_contents('../templates/email.html');
    //$emailOutput = str_replace('%name%', $name, $emailOutput);
    //$emailOutput = str_replace('%phone%', $name, $emailOutput);
    //$emailOutput = str_replace('%email%', $name, $emailOutput);

    $mail->Subject = '';
    $mail->isHTML(true);
    $mail->MsgHTML($emailOutput);
    $mail->AltBody = '';

    if (!$mail->send()) {
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        //close connection
        $stmt = null;
        $link = null;
    }

} else {
    // Not a POST request, set a 403 (forbidden) response code.
    http_response_code(403);
    echo "There was a problem with your submission, please try again.";
}
