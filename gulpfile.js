/*
 four top level functions

 gulp.task : defines tasks
 gulp.src : points to the file you want to use
 gulp.dest : points to output
 gulp.watch : two main forms. Both of which return an EventEmitter that emits change evetns.

 .pipe for chaining its output inot other plugins

 */

// packages
const gulp         = require('gulp'),
      gutil        = require('gulp-util'),
      sass         = require('gulp-sass'),
      concat       = require('gulp-concat'),
      sourcemaps   = require('gulp-sourcemaps'),
      autoprefixer = require('gulp-autoprefixer'),
      plumber      = require('gulp-plumber'),
      coffee       = require('gulp-coffee'),
      restart      = require('gulp-restart'),
      uglify       = require('gulp-uglify'),
      htmlmin      = require('gulp-htmlmin'),
      refresh      = require('gulp-refresh'),
      imagemin     = require('gulp-imagemin'),
      imageminGifsicle = require('imagemin-gifsicle'),
      imageminOptipng = require('imagemin-optipng'),
      imageminJpegtran = require('imagemin-jpegtran'),
      imageminSvgo = require('imagemin-svgo');

// define the default task and add the watch task to it
gulp.task('default', ['watch']);

// plumber
// gulp.src('./src/*.ext')
//     .pipe(plumber())
//     .pipe(coffee())
//     .pipe(gulp.dest('./public'));

// min js
gulp.task('build-js', function () {
    return gulp.src('src/js/main.js')
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        //only uglify if gulp is ran with '--type production'
        .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/js'))
        .pipe(refresh());
});

// scss
gulp.task('build-css', function () {
    return gulp.src('src/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/css'))
        .pipe(refresh());
});

// autoprefixer
gulp.task('autoprefixer', function () {
    gulp.src('src/css/main.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('public/css'));
});

// htmlmin
gulp.task('html-minify', function () {
    return gulp.src('src/*.html')
        .pipe(htmlmin({
            collapseWhitesoace: true,
            ignoreCustomFragments: [/<%[\s\S]*?%>/, /<\?[=|php]?[\s\S]*?\?>/]
        }))
        .pipe(gulp.dest('public'))
        .pipe(refresh());
});

// php copy classes
gulp.task('copy1', function () {
    gulp.src('src/php/classes/**/*.php')
        .pipe(gulp.dest('./public/php/classes'));
});

// php copy config
gulp.task('copy2', function () {
    gulp.src('src/php/config/**/*.php')
        .pipe(gulp.dest('./public/php/config'));
});

//copy php files
gulp.task('copy3', function () {
    gulp.src('src/**/*.php')
        .pipe(gulp.dest('./public'));
});

gulp.task('copy4', function () {
    gulp.src('src/js/libs/**/*.js')
        .pipe(gulp.dest('./public/js/libs'));
});

gulp.task('copy5', function () {
    gulp.src('src/js/vendor/**/*.js')
        .pipe(gulp.dest('./public/js/vendor'));
});

// img compression
gulp.task('img-min', () =>
    gulp.src('src/img/*')
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({plugins: [{removeViewBox: true}]})
        ]))
        .pipe(gulp.dest('public/img'))
);

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function () {
    refresh.listen({basePath: 'public', cache: false});
    refresh.listen({basePath: 'src', cache: false});
    gulp.watch('src/js/**/*.js', ['build-js']);
    gulp.watch('src/scss/**/*.scss', ['build-css']);
    gulp.watch('src/**/*.html', ['html-minify']);
    gulp.watch('src/css/**/*.css', ['autoprefixer']);
    gulp.watch('src/php/classes/**/*.php', ['copy1']);
    gulp.watch('src/php/config/**/*.php', ['copy2']);
    gulp.watch('src/**/*.php', ['copy3']);
    gulp.watch('src/js/libs/**/*.php', ['copy4']);
    gulp.watch('src/js/vendor/**/*.php', ['copy5']);
    gulp.watch('src/img/*', ['img-min']);
});