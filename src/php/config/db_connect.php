<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 13/12/2016
 * Time: 12:26
 */


//DB details
//Local
$dbUser   = "root";
$dbPS     = "root";
$dbName   = "db_dev";
$dbServer = "localhost";
$port     = "8889";

//Create connection
try {
    $link = new PDO("mysql:host=$dbServer;dbname=$dbName", $dbUser, $dbPS);
    //Set error to exception
    $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "db connect";

    //create db
//    $db_new = "CREATE DATABASE  newDB";
//    $link->exec($db_new);
//    echo "DB created";

    //create table
//    $db_table = "CREATE TABLE newTable(
//    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
//    name VARCHAR(64) COLLATE utf8_unicode_ci NOT NULL,
//    email VARCHAR(64) COLLATE utf8_unicode_ci NOT NULL,
//    phone VARCHAR(64) COLLATE utf8_unicode_ci NOT NULL,
//    reg_date TIMESTAMP,
//    UNIQUE KEY `phone` (`name`),
//    UNIQUE KEY `email` (`email`)
//    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ";
//    $link->exec($db_table);
//    echo "table created";

} catch (PDOException $e) {
    //throw error
    echo "Connect failed: " . $e->getMessage();
}
